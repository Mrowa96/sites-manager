# Sites manager

## Requirements
- php >= 7.3 with curl extension
- nodejs >= 10

## To run app
- `php composer.phar install`
- `cd assets`
- `npm i`
- `npm run build`
- `cd ../`
- `php bin/console server:run`

## To run tests
- `php bin/phpunit`