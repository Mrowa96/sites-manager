import SitesTable from './scripts/sites-table';
import { lockForm, unlockForm } from './scripts/helpers';
import './index.pcss';

document.addEventListener('DOMContentLoaded', function onLoaded() {
  const sitesUrlForm = document.querySelector('.sites-url-form');
  const loadLinksButton = document.querySelector('.load-links-button');
  const urlsField = document.getElementById('urls-field');
  let requestCounter = 0;

  const sitesTable = new SitesTable(
    document.querySelector('.sites-status-table'),
    function onSiteReload(table, rowId, url, callback) {
      fetch(`/check-site?url=${url}`)
        .then(function onResponse(response) {
          return response.json();
        })
        .catch(function onError() {
          table.updateRow(rowId, 'FAIL');
          callback();
        })
        .then(function onData(data) {
          data.status
            ? table.updateRow(rowId, 'OK', data)
            : table.updateRow(rowId, 'FAIL');
          callback();
        });
    },
  );

  if (urlsField) {
    if (loadLinksButton) {
      loadLinksButton.addEventListener('click', function() {
        urlsField.value =
          'https://www.x-kom.pl/\nhttps://pclab.pl/art73761-5.html\nhttps://www.tvn.pl/test,293262,n.html';
      });
    }

    sitesUrlForm.addEventListener('submit', function(event) {
      event.preventDefault();

      sitesTable.clearTable();

      const urls = urlsField.value
        .split('\n')
        .map(url => url.trim())
        .filter(url => !!url.length);

      urls.forEach(function(url) {
        const rowId = sitesTable.addRow(url);

        requestCounter++;

        lockForm(sitesUrlForm);

        fetch(`/check-site?url=${url}`)
          .then(function onResponse(response) {
            requestCounter--;

            if (requestCounter === 0) {
              unlockForm(sitesUrlForm);
            }

            return response.json();
          })
          .catch(function onError() {
            sitesTable.updateRow(rowId, 'FAIL');
          })
          .then(function onData(data) {
            data.status
              ? sitesTable.updateRow(rowId, 'OK', data)
              : sitesTable.updateRow(rowId, 'FAIL');
          });
      });
    });
  }
});
