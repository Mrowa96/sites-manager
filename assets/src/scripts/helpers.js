export function lockForm(form) {
  form.querySelector('button').setAttribute('disabled', 'disabled');
}

export function unlockForm(form) {
  form.querySelector('button').removeAttribute('disabled');
}
