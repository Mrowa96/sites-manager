import uuid from 'uuid';

class SitesTable {
  #tableNode;

  #onReload;

  constructor(tableNode, onReload) {
    if (!tableNode) {
      throw new Error('Table node have to be defined.');
    }

    if (!onReload) {
      throw new Error('onReload callback have to be defined.');
    }

    this.#tableNode = tableNode;
    this.#onReload = onReload;
  }

  addRow(url, status = 'PENDING') {
    const row = document.createElement('tr');
    const urlCell = document.createElement('td');
    const statusCell = document.createElement('td');
    const statusCodeCell = document.createElement('td');
    const redirectToCell = document.createElement('td');
    const reloadButtonCell = document.createElement('td');
    const reloadButton = document.createElement('button');

    urlCell.classList.add('url-cell');
    statusCell.classList.add('status-cell');
    statusCodeCell.classList.add('status-code-cell');
    redirectToCell.classList.add('redirect-to-cell');
    reloadButtonCell.classList.add('reload-button-cell');
    reloadButton.classList.add('reload-button');

    row.dataset.id = uuid.v4();
    urlCell.innerHTML = url;
    statusCell.innerHTML = status;

    reloadButton.innerHTML = 'Reload';
    reloadButton.addEventListener('click', () => {
      reloadButton.setAttribute('disabled', 'disabled');

      statusCell.innerHTML = 'PENDING';
      statusCodeCell.innerHTML = '';
      redirectToCell.innerHTML = '';

      this.#onReload(this, row.dataset.id, url, function() {
        reloadButton.removeAttribute('disabled');
      });
    });
    reloadButton.setAttribute('disabled', 'disabled');
    reloadButton.classList.add('button');

    reloadButtonCell.appendChild(reloadButton);
    row.appendChild(urlCell);
    row.appendChild(statusCell);
    row.appendChild(statusCodeCell);
    row.appendChild(redirectToCell);
    row.appendChild(reloadButtonCell);

    this.tableBodyNode.appendChild(row);

    return row.dataset.id;
  }

  updateRow(rowId, status, data) {
    const row = this.getRowById(rowId);

    row.querySelector('.status-cell').innerHTML = status;

    if (status === 'OK') {
      row.querySelector('.status-code-cell').innerHTML = data.statusCode;
      row.querySelector('.redirect-to-cell').innerHTML = data.redirectUrl
        ? data.redirectUrl
        : '-';
      row.querySelector('.reload-button').removeAttribute('disabled');
    } else if (status === 'FAIL') {
      row.querySelector('.reload-button').removeAttribute('disabled');
    }
  }

  clearTable() {
    this.tableBodyNode.querySelectorAll('tr').forEach(function(trNode) {
      trNode.remove();
    });
  }

  getRowById(rowId) {
    return this.tableBodyNode.querySelector(`[data-id="${rowId}"]`);
  }

  get tableBodyNode() {
    return this.#tableNode.querySelector('tbody');
  }
}

export default SitesTable;
