<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Twig\Environment;
use App\Service\ICheckSiteService;

final class DefaultController
{
    /** @var Environment */
    private $environment;

    /** @var ICheckSiteService */
    private $checkSiteService;

    public function __construct(ICheckSiteService $checkSiteService, Environment $environment)
    {
        $this->checkSiteService = $checkSiteService;
        $this->environment = $environment;
    }

    public function index(): Response
    {
        return new Response($this->environment->render('default/index.twig'));
    }

    public function checkSite(Request $request): JsonResponse
    {
        return new JsonResponse($this->checkSiteService->check($request->get('url', null)));
    }
}