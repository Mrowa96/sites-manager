<?php

namespace App\Model;

use JsonSerializable;

class SiteResult implements JsonSerializable
{
    /** @var bool */
    private $valid;

    /** @var int|null */
    private $statusCode;

    /** @var string|null */
    private $redirectUrl;

    public function __construct(bool $valid, ?int $statusCode = null, ?string $redirectUrl = null)
    {
        $this->valid = $valid;
        $this->statusCode = $statusCode;
        $this->redirectUrl = $redirectUrl;
    }

    public function isValid(): bool
    {
        return $this->valid;
    }

    public function getStatusCode(): ?int
    {
        return $this->statusCode;
    }

    public function getRedirectUrl(): ?string
    {
        return $this->redirectUrl;
    }

    public function jsonSerialize()
    {
        return [
            'status' => $this->valid,
            'statusCode' => $this->getStatusCode(),
            'redirectUrl' => $this->getRedirectUrl(),
        ];
    }
}