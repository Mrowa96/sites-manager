<?php

namespace App\Service;

use GuzzleHttp\Exception\GuzzleException;
use Psr\Log\LoggerInterface;
use App\Model\SiteResult;

final class CheckSiteService implements ICheckSiteService
{
    /** @var IHttpService */
    private $httpService;

    /** @var LoggerInterface */
    private $logger;

    public function __construct(IHttpService $httpService, LoggerInterface $logger)
    {
        $this->httpService = $httpService;
        $this->logger = $logger;
    }

    public function check(?string $url): SiteResult
    {
        if (empty($url)) {
            return new SiteResult(false);
        }

        try {
            $request = $this->httpService->request('GET', $url, [
                'verify' => false,
                'allow_redirects' => [
                    'max' => 10,
                    'strict' => true,
                    'referer' => true,
                    'track_redirects' => true,
                ],
            ]);

            $redirectStatuses = $request->getHeaderLine('X-Guzzle-Redirect-Status-History');
            $redirectUrls = $request->getHeaderLine('X-Guzzle-Redirect-History');

            if (mb_strlen($redirectStatuses) > 0 && mb_strlen($redirectUrls) > 0) {
                $statusCode = array_map(function (string $redirectStatus) {
                    return trim($redirectStatus);
                }, explode(",", $redirectStatuses))[0];
                $redirectUrl = array_map(function (string $redirectUrl) {
                    return trim($redirectUrl);
                }, explode(",", $redirectUrls))[0];
            } else {
                $statusCode = $request->getStatusCode();
                $redirectUrl = null;
            }

            return new SiteResult(true, $statusCode, $redirectUrl);
        } catch (GuzzleException $exception) {
            $this->logger->error($exception);

            return new SiteResult(false);
        }
    }
}