<?php

namespace App\Service;

use App\Model\SiteResult;

interface ICheckSiteService
{
    public function check(?string $url): SiteResult;
}