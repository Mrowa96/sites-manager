<?php

namespace App\Tests\Service;

use App\Service\CheckSiteService;
use App\Service\HttpService;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;

class CheckSiteServiceTest extends TestCase
{
    /** @var CheckSiteService */
    private $service;

    /** @var HandlerStack */
    private static $handler;

    public static function setUpBeforeClass()
    {
        $mock = new MockHandler([
            new Response(200),
            new Response(200, [
                'X-Guzzle-Redirect-History' => 'http://redirected-url.com',
                'X-Guzzle-Redirect-Status-History' => '301',
            ]),
        ]);

        self::$handler = HandlerStack::create($mock);
    }

    public function setUp()
    {
        $loggerMock = $this->createMock(LoggerInterface::class);
        $httpServiceMock = new HttpService(['handler' => self::$handler]);

        $this->service = new CheckSiteService($httpServiceMock, $loggerMock);
    }

    /** @test */
    public function itShouldReturnResponseWith200StatusCode()
    {
        $result = $this->service->check('http://any-url.com');

        $this->assertEquals($result->isValid(), true);
        $this->assertEquals($result->getStatusCode(), 200);
    }

    /** @test */
    public function itShouldReturnResponseWithRedirectUrlAnd301StatusCode()
    {
        $result = $this->service->check('http://any-url2.com');

        $this->assertEquals($result->isValid(), true);
        $this->assertEquals($result->getStatusCode(), 301);
        $this->assertEquals($result->getRedirectUrl(), 'http://redirected-url.com');
    }
}